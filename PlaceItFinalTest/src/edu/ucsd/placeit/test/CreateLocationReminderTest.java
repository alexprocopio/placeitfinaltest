package edu.ucsd.placeit.test;

import android.os.RemoteException;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;

public class CreateLocationReminderTest extends UiAutomatorTestCase {
	/*
	/**
     * Launches an app by it's name. 
     * 
     * Code source: http://blog.bettersoftwaretesting.com/2013/03/android-test-automation-getting-to-grips-with-ui-automator/
     * 
     * @param nameOfAppToLaunch the localized name, an exact match is required to launch it.
     */
	/*   protected static void launchAppCalled(String PlaceIt) throws UiObjectNotFoundException {
        UiScrollable appViews = new UiScrollable(new UiSelector().scrollable(true));
          // Set the swiping mode to horizontal (the default is vertical)
          appViews.setAsHorizontalList();
          appViews.scrollToBeginning(10);  // Otherwise the Apps may be on a later page of apps.
          int maxSearchSwipes = appViews.getMaxSearchSwipes();

          UiSelector selector;
          selector = new UiSelector().className(android.widget.TextView.class.getName());
          
          UiObject appToLaunch;
          
          // The following loop is to workaround a bug in Android 4.2.2 which
          // fails to scroll more than once into view.
          for (int i = 0; i < maxSearchSwipes; i++) {

              try {
                  appToLaunch = appViews.getChildByText(selector, PlaceIt);
                  if (appToLaunch != null) {
                      // Create a UiSelector to find the Settings app and simulate      
                      // a user click to launch the app.
                      appToLaunch.clickAndWaitForNewWindow();
                      break;
                  }
              } catch (UiObjectNotFoundException e) {
                  System.out.println("Did not find match for " + e.getLocalizedMessage());
              }

              for (int j = 0; j < i; j++) {
                  appViews.scrollForward();
                  System.out.println("scrolling forward 1 page of apps.");
              }
          }
    }
*/
	public void CreateLocRemTest() throws UiObjectNotFoundException, RemoteException {
		
		//creates the UI device object to use for User Inputs on screen
		//UiDevice device = getUiDevice();
		//device.pressHome();
		//device.pressRecentApps(); in case not already in PlaceIt app will find in recents
	
		//object for Place-it app made to find in recents menu
		UiObject placeIt = new UiObject(new UiSelector() 
			.className("android.widget.TextView")
			.text("Place it"));
	
		//open Place-it
		placeIt.clickAndWaitForNewWindow();
		
		
		//Create objects for UI testing
		
		UiObject map = new UiObject(new UiSelector()
			.className("android.widget.RelativeLayout")
			.index(0));

		UiObject newButton = new UiObject(new UiSelector()
			.className("android.widget.Button")
			.text("New Category PlaceIt"));
		
		UiObject enterText = new UiObject(new UiSelector()
			.text("task name..")
			.className("android.widget.EditText"));

		UiObject text2 = new UiObject(new UiSelector()
			.className("android.widget.EditText")
			.index(0));
		
		UiObject firstCategory = new UiObject(new UiSelector()
			.className("android.widget.Spinner")
			.resourceId("id/new_task_category_1"));
		
		UiObject secondCategory = new UiObject(new UiSelector()
			.className("android.widget.Spinner")
			.resourceId("id/new_task_category_2"));
		
		UiObject thirdCategory = new UiObject(new UiSelector()
			.className("android.widget.Spinner")
			.resourceId("id/new_task_category_3"));

		UiObject subButton = new UiObject(new UiSelector()
			.className("android.widget.Button")
			.text("New Category PlaceIt"));

		UiObject chooseFirstCategory = new UiObject(new UiSelector()
			.className("android.widget.CheckedTextView")
			.index(1));
		
		UiObject chooseSecondCategory = new UiObject(new UiSelector()
			.className("android.widget.CheckedTextView")
			.index(2));
	
		UiObject chooseThirdCategory = new UiObject(new UiSelector()
			.className("android.widget.CheckedTextView")
			.index(3));
	
		UiObject noCategory = new UiObject(new UiSelector()
			.className("android.widget.TextView")
			.text("Please enter a name for the place-it."));
		
		UiObject okButton = new UiObject(new UiSelector()
			.className("android.widget.Button")
			.text("OK"));
		
		
		/*		Scenario: User creates reminder using a single category of a location
		 *
		 *		Given: That user is using Place-its, is logged in, and is creating a reminder
		 *		When: The user chooses a single category (e.g. Grocery Store)
		 *		Then: A reminder is created for all locations of that category
		 */
		
	
		newButton.clickAndWaitForNewWindow();		
		enterText.setText("Get groceries");
		text2.click();
		text2.setText("Almond milk, pinto beans, brown rice, oranges, apples");
		firstCategory.click();
		chooseFirstCategory.click();
		subButton.clickAndWaitForNewWindow();
		assertTrue("Not returned to map", map.exists());
	
		
/*		Scenario: User creates reminder not using a category of a location
 *		Given: That user is using Place-its, is logged in, and is creating a reminder
 *		When: The user does not choose a category
 *		Then: The user will be sent to the map to choose a specific location for the reminder being created
 *		And when location is selected a reminder would be created for that specific location and not all locations of the same category
 */
		
		newButton.clickAndWaitForNewWindow();		
		enterText.setText("Get cash");
		text2.click();
		text2.setText("Get enough cash for the trip, there will be cash only vendor on the way");
		subButton.clickAndWaitForNewWindow();
		assertTrue("No error for invalid fields", noCategory.exists());
		okButton.clickAndWaitForNewWindow();
		
/*		Scenario: User creates reminder using more than one category of a location
 *		Given: That user is using Place-its and is creating a reminder
 *		When: The user selects more than one category for the place-it (up to three)
 *		Then: A reminder will be created with the categories selected
 */
		firstCategory.click();
		chooseFirstCategory.click();
		secondCategory.click();
		chooseSecondCategory.click();
		thirdCategory.click();
		chooseThirdCategory.click();
		subButton.clickAndWaitForNewWindow();
		assertTrue("Not returned to map", map.exists());
		


		
	}
	
}
